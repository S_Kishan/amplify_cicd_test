export type AmplifyDependentResourcesAttributes = {
    "api": {
        "amplifycicdtest": {
            "GraphQLAPIKeyOutput": "string",
            "GraphQLAPIIdOutput": "string",
            "GraphQLAPIEndpointOutput": "string"
        }
    }
}